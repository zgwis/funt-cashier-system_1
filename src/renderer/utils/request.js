import axios from 'axios'
import { Notification, Message } from 'element-ui'
import { getToken } from '@/utils/auth'

const serves = axios.create({
  baseURL: process.env.userConfig.API_HOST,
  timeout: 5000
})

// 设置请求发送之前的拦截器
serves.interceptors.request.use(config => {
  // 设置发送之前数据需要做什么处理
  if (getToken()) {
      config.headers['Access-Token'] = getToken();
  }

  return config
}, err => Promise.reject(err))

// 设置请求响应拦截器
serves.interceptors.response.use(res => {
  // 响应代码
  const code = res.data.code || 200;
  // 获取错误信息
  const msg = res.data.message || "未知错误";

  if (code === 500) {
      Message.error(res.data.data);
  } else if (code !== 200) {
      Notification.error({
        title: msg
      })
      return Promise.reject('error');
  } else {
      return res.data;
  }
}, err => {
  // 判断请求异常信息中是否含有超时timeout字符串
  if (err.message.includes('timeout')) {
      console.log('错误回调', err)
      Message.error('网络超时')
  }
  if (err.message.includes('Network Error')) {
      console.log('错误回调', err)
      Message.error('服务端未启动，或网络连接错误')
  }
  return Promise.reject(err)
})

// 将serves抛出去
export default serves
